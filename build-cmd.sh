#!/bin/sh

# turn on verbose debugging output for parabuild logs.
set -x
# make errors fatal
set -e

if [ -z "$AUTOBUILD" ] ; then 
    fail
fi

if [ "$OSTYPE" = "cygwin" ] ; then
    export AUTOBUILD="$(cygpath -u $AUTOBUILD)"
fi

top="$(dirname "$0")"
STAGING_DIR="$(pwd)"
echo $STAGING_DIR
GLOD_VERSION=1.0pre4
build=${AUTOBUILD_BUILD_ID:=0}
echo "${GLOD_VERSION}.${build}" > "${STAGING_DIR}/VERSION.txt"
cd $top

# load autbuild provided shell functions and variables
set +x
eval "$("$AUTOBUILD" source_environment)"
set -x

build_linux()
{
  # Prefer gcc-4.6 if available.
  if [[ -x /usr/bin/gcc-4.6 && -x /usr/bin/g++-4.6 ]]; then
    export CC=/usr/bin/gcc-4.6
    export CXX=/usr/bin/g++-4.6
  fi
  libdir="${STAGING_DIR}/lib"
  mkdir -p "${STAGING_DIR}/lib"/debug
  mkdir -p "${STAGING_DIR}/lib"/release
  export CFLAGS="-m$1 $2"
  export LFLAGS="-m$1"
  make -C src clean
  make -C src debug
  cp "lib/libGLOD.so" \
    "$libdir/debug/libGLOD.so"
  make -C src clean
  make -C src release
  cp "lib/libGLOD.so" \
    "$libdir/release/libGLOD.so"
}

case "$AUTOBUILD_PLATFORM" in
    "windows")
        build_sln "glodlib.sln" "Debug|Default"
        build_sln "glodlib.sln" "Release|Default"
		mkdir -p $stage/lib/{debug,release}
        cp "lib/debug/glod.lib" \
            "$stage/lib/debug/glod.lib"
        cp "lib/debug/glod.dll" \
            "$stage/lib/debug/glod.dll"
        cp "src/api/debug/glod.pdb" \
            "$stage/lib/debug/glod.pdb"
        cp "lib/release/glod.lib" \
            "$stage/lib/release/glod.lib"
        cp "lib/release/glod.dll" \
            "$stage/lib/release/glod.dll"
    ;;
        "darwin")
			libdir="$top/$stage/lib"
            mkdir -p "$libdir"/{debug,release}
			make -C src clean
			make -C src debug
			install_name_tool -id "@executable_path/../Resources/libGLOD.dylib" "lib/libGLOD.dylib" 
			cp "lib/libGLOD.dylib" \
				"$libdir/debug/libglod.dylib"
			make -C src clean
			make -C src release
			install_name_tool -id "@executable_path/../Resources/libGLOD.dylib" "lib/libGLOD.dylib" 
			cp "lib/libGLOD.dylib" \
				"$libdir/release/libglod.dylib"
		;;
        "linux")
            build_linux 32
        ;;
        "linux64")
            build_linux 64 -fPIC
        ;;
esac
mkdir -p "$STAGING_DIR/include/glod"
cp "include/glod.h" "$STAGING_DIR/include/glod/glod.h"
mkdir -p $STAGING_DIR/LICENSES
cp LICENSE $STAGING_DIR/LICENSES/GLOD.txt



pass

